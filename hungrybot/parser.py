# -*- coding: utf-8 -*-

"""
Since lunchtime.cz doesn't have public API (such a shame), we need
to use HTML parser.
"""

import re
import codecs
import unicodedata
from lxml.html import parse


DEFAULT_URL = 'http://www.lunchtime.cz/denni-menu-v-okoli/brno-purkynova-99/'


# taken from http://code.activestate.com/recipes/576648-remove-diatrical-marks-including-accents-from-stri/
def remove_diacritic(inputstr):
    """
    Accept a unicode string, and return a normal string (bytes in Python 3)
    without any diacritical marks.
    """
    return unicodedata.normalize('NFKD', inputstr).encode('ASCII', 'ignore')


class LunchTimeParser(object):
    def __init__(self, url=DEFAULT_URL):
        self.url = url

    def _parse(self, restaurant):
        """
        Parse the page and return list of meals in format
        [(u'Meal', u'5Kc'), ..]
        """
        def _parse_item(main_container):
            item = []
            for part in meal.cssselect(main_container):
                if part is None:
                    continue
                cleaned = clean_regexp.sub(u'', part.text_content())
                price = price_regexp.search(cleaned)
                if price:
                    cleaned = '%s%s' % (price.group('price'),
                                        price.group('currency'))
                item.append(unicode(cleaned.encode('utf-8'), encoding='utf-8'))
            return item

        result = []
        rest_regexp = re.compile(r'%s' % restaurant.lower())
        clean_regexp = re.compile(r'(^\s+|\s+$)', flags=re.UNICODE)
        price_regexp = re.compile(r'(?P<price>\d+)\s+(?P<currency>\w+)?',
                                  flags=re.UNICODE)
        content = parse(self.url).getroot()
        for div in content.cssselect('div.restaurant'):
            headline = div.cssselect('h3.entry-title a')
            if headline is None:
                continue
            name = headline[0].text_content()
            if not rest_regexp.search(name.lower()):
                continue
            for meal in div.cssselect('table.tab-menu tr'):
                # parse normal restaurants
                item = _parse_item('td')
                # parse in case this is special restaurant
                if not item:
                    item = _parse_item('th')
                result.append(tuple(item))
        return result

    def _format(self, result):
        """
        Since twisted doesn't support unicode, we have to convert result
        to str.
        """
        diacritic_remover = lambda meal : tuple([str(remove_diacritic(part))
                                                 for part in meal])
        return [diacritic_remover(meal) for meal in result]

    def parse(self, restaurant):
        result = self._parse(restaurant)
        return self._format(result)
