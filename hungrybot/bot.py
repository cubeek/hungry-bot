# -*- coding: utf-8 -*-

import re
from twisted.words.protocols import irc
from twisted.internet import reactor, protocol

from . import answers
from .parser import DEFAULT_URL


class Dispatcher(object):
    msg_re = r'^\s*%s\s*[\:\,\>]\s*(?P<message>\w+[\s\w\$]*)'
    default = 'default'

    def __init__(self, name, module):
        self.name = name
        self.answers = module
        self.regexp = re.compile(self.msg_re % name)

    def dispatch(self, user, channel, msg):
        match = self.regexp.match(msg)
        if not match:
            return None
        message = match.group('message')
        parts = message.split('$', 1)

        method = re.sub(r'\s+', '_', parts[0]).strip('_')
        args = len(parts) > 1 and re.findall('\$[\w_]+', '$%s' % parts[1]) or []

        try:
            func = getattr(self.answers, method)
        except AttributeError:
            func = getattr(self.answers, self.default)
            msg = method
        if not callable(func):
            return None
        return func(user, channel, msg, *args)


class Bot(irc.IRCClient):
    nickname = 'hungry-bot'

    def signedOn(self):
        if self.nickname != self.factory.name:
            self.setNick(self.factory.name)
        for chan in self.factory.channels:
            self.join(chan)

    def joined(self, channel):
        self.say(channel, 'Yo wassup dudes?!')
        self.topic(channel, topic=DEFAULT_URL)

    def alterCollidedNick(self, nickname):
        return nickname + '_'

    def privmsg(self, user, channel, msg):
        user = user.split('!')[0]
        answer = self.factory.dispatcher.dispatch(user, channel, msg)
        if answer:
            for line in answer:
                self.say(channel, '%s: %s' % (user, line))


class BotFactory(protocol.ClientFactory):
    def __init__(self, name, channels):
        self.name = name
        self.channels = channels
        self.dispatcher = Dispatcher(name, answers)

    def buildProtocol(self, addr):
        prot = Bot()
        prot.factory = self
        return prot

    def clientConnectionLost(self, connector, reason):
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        reactor.stop()


def main(server, port, name, channels):
    # create factory protocol and application
    fact = BotFactory(name, channels)

    # connect factory to this host and port
    reactor.connectTCP(server, port, fact)

    # run bot
    reactor.run()
