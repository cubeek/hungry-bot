# -*- coding: utf-8 -*-

"""
Set of answers.
"""

from .parser import LunchTimeParser


__all__ = ('default', 'info', 'wtf', 'show')


def default(user, channel, msg, *args):
    return ['Yo, dunno wha\'are you talkin\'bout. '
            'Cannot find method "%s".' % msg]


def you_suck(user, channel, msg, *args):
    git_link = 'https://github.com/paramite/hungry-bot'
    return ['Yo, show me some luv. '
            'Patches are welcome %s ;).' % git_link]


def wtf(user, channel, msg, *args):
    return ['Yo, me are just stupid bot. '
            'Me gonna help ya find some food. '
            'Just write me "show $<restaurant_name>".']


def show(user, channel, msg, *args):
    msg = []
    parser = LunchTimeParser()
    for rest in args:
        restaurant = rest.strip('$ ')
        result = parser.parse(restaurant)
        if result:
            msg.append('Yo, me found somethin\' for %s:' % restaurant)
            for item in result:
                msg.append(' '.join(list(item)))
    if not msg:
        msg.append('Yo, me so stupid me find nothing for %s' % list(args))
    return msg
